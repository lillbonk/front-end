fetch("http://localhost:8000")
  .then(response => {
    return response.json();
  })
  .then(data => {
    // console.log(data);
    generateTable(data);
  });

async function generateTable(data) {
  data.forEach(x =>{
      console.log(x);

      let elTbl = document.getElementById('tableData');

      let elTitle = document.createElement('td');
      elTitle.innerText = x.title;
  
      let elText = document.createElement('td');
      elText.innerText = x.text;
      
      let elDate = document.createElement('td');
      elDate.innerText = x.created_at;

      let elRow = document.createElement('tr');
  
      elTbl.appendChild(elTitle);
      elTbl.appendChild(elText);
      elTbl.appendChild(elDate);
      elTbl.appendChild(elRow);
      
  });


}

elCreateJBTn = document.getElementById("createJournalBtn");
elCreateJeBtn = document.getElementById("createJournalEntryBtn");

elCreateJBTn.addEventListener("click", function() {
  location.href = "./create-journal.html";
});

elCreateJeBtn.addEventListener("click", function() {
  location.href = "./create-entry.html";
});
